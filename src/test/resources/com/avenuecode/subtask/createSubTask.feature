#language: en
#encoding: UTF-8
Feature: As a ToDo App user
  I should be able to create a subtask
  So I can break down my tasks in smaller pieces

  Scenario: Create subtask
    Given I have a task created
    And I click at the button Manage Subtasks
    When I create a subtask with description "Qa Avenue code subtask"
    Then the subtask should be appened on the list of created subtasks

  Scenario: Edit subtask
    Given I have a subtask created
    When I set a description: "New subtask description teste" for the subtask
    Then I have a new description for the subtask

  Scenario: Remove subTask
    Given I have a subtask created
    When I remove the created subtask
    Then the subtask shouldn't be append on the list
