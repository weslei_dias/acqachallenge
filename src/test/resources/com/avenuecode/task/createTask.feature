#language: en
#encoding: UTF-8
Feature: As a ToDo App user
    	 I should be able to create a task
   		 So I can manage my tasks

  Scenario: Create task
    Given I access the website "http://qa-test.avenuecode.com/"
    And I sign in with the credentials "quaminator@hotmail.com", "F1r3w4ll69"
    And I click at the button My Tasks
    Then I see my tasks ToDo list
    When I create a task with descriprion "Qa Avenue Code test"
    Then the task should be appended on the list of created tasks

  Scenario: Edit task
    Given I have a task created
    When I set a description: "New description test" for the task "Edit/Remove Description"
    Then I have a new description for the task

  Scenario: Remove task
   Given I have a task created
   When I remove the created task
   Then the task shouldn't be append on the list
