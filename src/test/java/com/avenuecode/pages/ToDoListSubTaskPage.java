package com.avenuecode.pages;

import com.avenuecode.reuse.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * All the actions of {@link ToDoListSubTaskPage}.
 * @author Wesley Dias
 *
 */
public class ToDoListSubTaskPage extends AbstractPage {

	private WebDriver driver;

	public ToDoListSubTaskPage(WebDriver driver) {
		super(driver);
	}

	public ToDoListSubTaskPage createSubTask(String description) {
		fillField("new_sub_task", description);
		getElementById("add-subtask").click();
		return this;
	}

	public ToDoListSubTaskPage verifySubTaskInPanel() {
		WebElement panelSubTaskElement = getElementByClassName("col-md-8");
		if (panelSubTaskElement.isDisplayed()){
			getLogger().info("Element added");
		} else {
			getLogger().info("Element not added");
		}

		return this;
	}

	public ToDoListSubTaskPage editDescription(String newDescription) {
		WebElement editElement = getElementByClassName("col-md-8");
		editElement.click();
		fillField("editable-hide", newDescription);
		getElementByClassName("glyphicon-ok").click();
		return this;
	}

	public ToDoListSubTaskPage verifyNewDescription() {
		WebElement element = getElementByCssSelector("a['#']");

		if (element.isDisplayed()){
			getLogger().info("Element added");
		} else {
			getLogger().info("Element not added");
		}
		return this;
	}

	public ToDoListSubTaskPage removeSubTask() {
		WebElement element = getElementByClassName("btn-danger");
		element.click();
		return this;
	}

	public ToDoListSubTaskPage verifySubTaskVisibility() {
		String text = getDriver()
				.findElement(
						By.xpath("html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr/td[2]/a"))
				.getText();

		if (text.equals("Edit/Remove Description")) {
			getLogger().info("Element not removed");
		} else {
			getLogger().info("Element removed");
		}

		return this;
	}

	public ToDoListSubTaskPage driveButtonManageSubtask() {
		WebElement btManageSubtask = getElementByClassName("btn-primary");
		btManageSubtask.click();
		return this;
	}

}
