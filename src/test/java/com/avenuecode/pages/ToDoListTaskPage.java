package com.avenuecode.pages;

import com.avenuecode.reuse.AbstractPage;
import cucumber.runtime.CucumberException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * All the actions of the page {@link ToDoListTaskPage}.
 * @author Wesley Dias
 *
 */
public class ToDoListTaskPage extends AbstractPage {

	public ToDoListTaskPage(WebDriver driver) {
		super(driver);
	}

	public ToDoListTaskPage verifyTodoList() {
		WebElement element = getElementByClassName("panel-heading");

		if (element != null) {
			getLogger().info("Elements panel found");
		} else {
			throw new CucumberException("Element not found!");
		}

		return this;
	}

	public ToDoListTaskPage createTask(String description) {
		fillField("new_task", description);
		getElementByClassName("input-group-addon").click();
		return this;
	}

	public ToDoListTaskPage verifyTaskInPanel() {
		verifyIfElementIsDisplayedWithClass("task_body");
		return this;
	}

	public ToDoListTaskPage editDescription(String newDescription, String oldDescription) {
		List<WebElement> elements = getElementsByClass("task_body");
		WebElement newElement = null;

		for (WebElement element: elements) {
			if (element.getAttribute("outerText").equals(oldDescription)) {
				newElement = element;
				break;
			}
		}

		newElement.sendKeys(newDescription);

		return this;
	}

	public ToDoListTaskPage verifyNewDescription(String description) {
		List<WebElement> elements = getElementsByClass("task_body");
		WebElement newElement = null;

		for (WebElement element: elements) {
			if (element.getAttribute("outerText").equals(description)) {
				newElement = element;
				break;
			}
		}
		newElement.isDisplayed();
		return this;
	}

	public ToDoListTaskPage removeTask() {
		getElementByClassName("ng-scope").click();
		return this;
	}

	public ToDoListTaskPage verifyTaskVisibility() {
		List<WebElement> elements = getElementsByClass("task_body");
		WebElement newElement = null;

		for (WebElement element: elements) {
			if (element.getAttribute("outerText").equals("Edit/Remove Task")) {
				newElement = element;
				break;
			}
		}
		if (newElement != null) {
			getLogger().info("Element not removed");
		} else {
			getLogger().info("Element removed");
		}
		return this;
	}
}
