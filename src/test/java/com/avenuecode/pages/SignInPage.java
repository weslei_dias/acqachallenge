package com.avenuecode.pages;

import com.avenuecode.reuse.AbstractPage;
import org.openqa.selenium.WebDriver;

/**
 * Sign in page.
 * @author Wesley Dias
 *
 */
public class SignInPage extends AbstractPage {

	public SignInPage(WebDriver driver) {
		super(driver);
	}

	public SignInPage acess(String webSite) {
		accessUrl(webSite);
		return this;
	}

	public SignInPage acessSignInPage() {
		accessUrl("http://qa-test.avenuecode.com/users/sign_in");
		return this;
	}

	public SignInPage signIn(String email, String password) {
		fillField("user_email", email);
		fillField("user_email", password);
		getElementByClassName("btn-primary").click();
		return this;
	}

	public SignInPage clickButtonMyTask() {
		getElementByClassName("btn-success").click();
		return this;
	}

}
