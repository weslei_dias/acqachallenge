package com.avenuecode.factory;

import com.avenuecode.pages.SignInPage;
import com.avenuecode.pages.ToDoListSubTaskPage;
import com.avenuecode.pages.ToDoListTaskPage;

/**
 * Factory of task and subtasks.
 * @author Wesley Dias
 *
 */
public class ToDoListFactory {
	
	private SignInPage signInPage;
	private ToDoListTaskPage toDoListPage;
	private ToDoListSubTaskPage toDoListSubTaskPage;
	
	public ToDoListFactory(SignInPage signInPage, 
			ToDoListTaskPage toDoListPage, 
			ToDoListSubTaskPage toDoListSubTaskPage) {
		this.signInPage = signInPage;
		this.toDoListPage = toDoListPage;
		this.toDoListSubTaskPage = toDoListSubTaskPage;
	}
	
	public void createTask()
	{
		signInPage.acessSignInPage();
		signInPage.signIn("quaminator@hotmail.com", "F1r3w4ll69");
		signInPage.clickButtonMyTask();
		toDoListPage.createTask("Edit/Remove Task");
	}
	
	public void createSubTask()
	{
		createTask();
		toDoListSubTaskPage.driveButtonManageSubtask();
		toDoListSubTaskPage.createSubTask("Edit/Remove Description");
	}

}
