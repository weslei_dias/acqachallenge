package com.avenuecode.stepdefs;

import com.avenuecode.factory.ToDoListFactory;
import com.avenuecode.pages.SignInPage;
import com.avenuecode.pages.ToDoListSubTaskPage;
import com.avenuecode.pages.ToDoListTaskPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;

/**
 * This class contains all the steps of the created features.
 * 
 * @author Wesley Dias
 *
 */
public class ToDoAppStepDefs {
	private WebDriver driver;

	private SignInPage signPage;
	private ToDoListTaskPage toDoListTaskPage;
	private ToDoListSubTaskPage toDoListSubTaskPage;
	private ToDoListFactory toDoListFactory;

	public ToDoAppStepDefs() {
		final File file = new File("src/test/resources/driver/geckodriver");
		System.setProperty("webdriver.gecko.driver", file.getAbsolutePath());
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		signPage = new SignInPage(driver);
		toDoListTaskPage = new ToDoListTaskPage(driver);
		toDoListSubTaskPage = new ToDoListSubTaskPage(driver);
		toDoListFactory = new ToDoListFactory(signPage, toDoListTaskPage,
				toDoListSubTaskPage);
	}

	@Given("I access the website \"(.*)\"")
	public void acess(String webSite) {
		signPage.acess(webSite);
		signPage.acessSignInPage();
	}

	@Given("I sign in with the credentials \"(.*)\", \"(.*)\"")
	public void signIn(String email, String password) {
		signPage.signIn(email, password);

	}

	@Given("I click at the button My Tasks")
	public void clickButtonMyTask() {
		signPage.clickButtonMyTask();
	}

	@Then("I see my tasks ToDo list")
	public void verifyTodoList() {
		toDoListTaskPage.verifyTodoList();
	}

	@When("I create a task with descriprion \"(.*)\"")
	public void createTask(String description) {
		toDoListTaskPage.createTask(description);
	}

	@Then("the task should be appended on the list of created tasks")
	public void verifyTaskInPanel() {
		toDoListTaskPage.verifyTaskInPanel();
	}

	@Given("I have a task created")
	public void createTask() {
		toDoListFactory.createTask();
	}

	@When("I set a description: \"(.*)\" for the task \"(.*)\"")
	public void editTask(String newDescription, String oldDescription) {
		toDoListTaskPage.editDescription(newDescription, oldDescription);
	}

	@Then("I have a new description for the task")
	public void verifyNewDescription() {
		toDoListTaskPage.verifyNewDescription("New description test");
	}

	@When("I remove the created task")
	public void removeTask() {
		toDoListTaskPage.removeTask();
	}

	@Then("the task shouldn't be append on the list")
	public void verifyTaskVisibility() {
		toDoListTaskPage.verifyTaskVisibility();
	}
	
/**************************SubTask**********************************/
	
	@Given("I click at the button Manage Subtasks")
	public void clickManegeSubtask()
	{
		toDoListSubTaskPage.driveButtonManageSubtask();
	}
	
	@When("I create a subtask with description \"(.*)\"")
	public void createSubtask(String description)
	{
		toDoListSubTaskPage.createSubTask(description);
	}
	
	@Then("the subtask should be appened on the list of created subtasks")
	public void verifyCreatedSubtasks()
	{
		toDoListSubTaskPage.verifySubTaskInPanel();
	}
	
	@When("I set a description: \"(.*)\" for the subtask")
	public void editSubtask(String newDescription) {
		toDoListSubTaskPage.editDescription(newDescription);
	}

	@Then("I have a new description for the subtask")
	public void verifyNewDescriptionSubtask() {
		toDoListSubTaskPage.verifyNewDescription();
	}

	@When("I remove the created subtask")
	public void removesubtask() {
		toDoListSubTaskPage.removeSubTask();
	}

	@Then("the subtask shouldn't be append on the list")
	public void verifySubtaskVisibility() {
		toDoListSubTaskPage.verifySubTaskVisibility();
	}
	
	@Given("I have a subtask created")
	public void createSubTask() {
		toDoListFactory.createSubTask();
	}

}
